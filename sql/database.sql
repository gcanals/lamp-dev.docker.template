

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";



--
-- Base de données: `sample`
--


USE `sample`;



CREATE TABLE `restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `adresse` text NOT NULL,
  `contact` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `restaurant`
--

INSERT INTO `restaurant` (`id`, `nom`, `description`, `adresse`, `contact`) VALUES
(1, 'Chez Mario et Luigi', 'Les pizzaiolos du jeu video !!', '8 rue sous le tuyau.\r\nMarioLand', 'mario@marioland'),
(2, 'Fu Mange Tout', 'Le meilleur restaurant chinois de toute la ville.', 'Shangai', 'Fu@MangTout'),
(3, 'Les sushis sont secs', 'Une tradition directement venue du Japon. Tous les sushis de vos souhaits !', '32 Rue de Totoro\r\nNancy', 'Sushi@sonsecs'),
(4, 'SMSexicain', 'Le meilleur restaurant de TextMex. Tapas et Fajitas à volonté (et plus encore).\r\n\r\nPour ceux qui n''ont pas froid aux yeux', '14 rue de la grand place\r\nLaxouVille\r\n', 'sms@xicain'),
(7, 'Pizza the Hut', 'La seule pizzeria qui possède l''achtusse.', '2 impasse de la galaxie\r\nLuxembourgVille', 'pizza@thehut'),
(8, 'Yamazaki', 'De nombreux sushis fait avec amour et avec le poisson le plus frais du marché.', '5 Grand Rue\r\nMalzéville', 'yama@saki'),
(9, 'Little Pakistan', 'Délices du Pakistan', '12 rue du Faubour des Trois Maison, 54000, nancy', 'little@pakistan'),
(10, 'Le Taj Mahal', 'On aime les épices', '45 rue des Fabrique, 54000 Nancy', 'taj@mahal');


