#!/bin/bash


if [ -z "$MYSQL_ROOT_PWD" -a -z "$MYSQL_ALLOW_EMPTY_PASSWORD" ]; then
			echo >&2 'error: MYSQL_ROOT_PWD not set'
			echo >&2 '  Did you forget to add -e MYSQL_ROOT_PWD=... ?'
			exit 1
fi



echo "configuring database"

# starting mysqld daemon for local usage
/usr/sbin/mysqld &
sleep 5

sqlinitfile=$(mktemp /tmp/mysql-init-first.XXXXXX.sql)

cat > "$sqlinitfile" <<-EOSQL


      CREATE USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PWD}' ;
			GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;

			DROP DATABASE IF EXISTS test ;
EOSQL


if [ "$DBNAME" ]; then
  echo "CREATE DATABASE IF NOT EXISTS $DBNAME ;"
  echo "CREATE DATABASE IF NOT EXISTS $DBNAME ;" >> "$sqlinitfile"
fi

if [ "$DBUSER" -a "$DBPWD" ]; then
  echo "CREATE USER '"$DBUSER"'@'%' IDENTIFIED BY '"$DBPWD"' ;"
  echo "CREATE USER '"$DBUSER"'@'%' IDENTIFIED BY '"$DBPWD"' ;" >> "$sqlinitfile"
  echo "CREATE USER '"$DBUSER"'@'localhost' IDENTIFIED BY '"$DBPWD"' ;" >> "$sqlinitfile"

  if [ "$DBNAME" ]; then
    echo "GRANT ALL ON "$DBNAME".* TO '"$DBUSER"'@'%' ;"
    echo "GRANT ALL ON "$DBNAME".* TO '"$DBUSER"'@'%' ;" >> "$sqlinitfile"
    echo "GRANT ALL ON "$DBNAME".* TO '"$DBUSER"'@'localhost' ;" >> "$sqlinitfile"
  fi
fi

echo 'FLUSH PRIVILEGES ;' >> "$sqlinitfile"

if [ "$DBINIT" ]; then
  echo " adding database init content"
  cat /database.sql >> "$sqlinitfile"
fi

mysql --default-character-set=utf8 -uroot < "$sqlinitfile"


#rm -f "$sqlinitfile"

#echo "setting root password"

#mysqladmin -u root password $MYSQL_ROOT_PWD -p ""
mysqladmin shutdown
echo "finished"


echo "Starting MySQL Server"
/usr/sbin/mysqld
#/bin/bash
