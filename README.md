# Lamp dev docker template #

[docker]: https://dev.mysql.com/downloads/workbench/

### Un ensemble de conteneurs docker pour du dev web en php ###

Ce dépôt propose un cadre docker multi-container de base pour développer une application web lamp. 3 services sont gérés et déployés à l'aide de docker-compose :  

- **mydb** : un service mysql pour gérer les données de l'application
- **data** : un service de stockage de fichiers permetttant à l'application d'utiliser des données éventuellement partagées avec d'autres applications,
- **http** : un service apache-php, 


__Attention : Ne pas utiliser en environement de production__

## Prérequis ##

Tout d'abord, installer [Docker](https://docs.docker.com/), et [docker-compose](https://docs.docker.com/compose/install/), l'outil de gestion d'application multi-container.


Pour les développeurs travaillant sur  __Windows__  ou __Mac OS X__ il faut auparavant 
installer  [boot2docker](http://boot2docker.io)  : 

- [Windows](https://github.com/boot2docker/windows-installer/releases) 
- [Mac OS X](https://github.com/boot2docker/osx-installer/releases)

## Usage ##

La configuration des différents services se fait en éditant le fichier docker-compose.yml. 
La construction des images se fait à l'aide de la commande :


```Shell
bash-3.2$ docker-compose build
```

2 images sont construites : une pour le service **mydb** et une pour le service **http** : 

```Shell
bash-3.2$ docker images
REPOSITORY                   TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
lampdevdockertemplate_mydb   latest              8df14534f840        19 hours ago        318.1 MB
lampdevdockertemplate_http   latest              e3e80be96639        21 hours ago        268.7 MB
```
Le démarrage des services se fait grâce à la commande : 

```Shell
docker-compose up -d 
Recreating lampdevdockertemplate_mydb_1...
Recreating lampdevdockertemplate_data_1...
Recreating lampdevdockertemplate_http_1...
```
ce qui conduit à la création et l'activation des 3 conteneurs correspondant aux 3 services : 

```Shell
bash-3.2$ docker-compose ps
            Name                         Command              State            Ports          
---------------------------------------------------------------------------------------------
lampdevdockertemplate_data_1   /bin/bash                      Exit 0                          
lampdevdockertemplate_http_1   /run.sh                        Up       0.0.0.0:80->80/tcp     
lampdevdockertemplate_mydb_1   /usr/local/bin/entrypoint.sh   Up       0.0.0.0:3306->3306/tcp 
```
Vous pouvez vérifier que les services sont opérationnels en les interrogeant. Pour cela, se connecter au service mysql ou au service http sur la machine locale 127.0.0.1 pour les utilisateurs linux ou sur la machine virtuelle boot2docker pour les utilisateurs windows/mac OS X.

Pour connaitre l'adresse ip de la machine boot2docker : 
```Shell
bash-3.2$ boot2docker ip
192.168.59.103
```

**Pour tester le service http :**
```Shell
bash-3.2$ curl http://127.0.0.1 # ou l'@ip de la machine boot2docker
<html>
<head>
	<title>Hello world!</title>
</head>
<body>
...
```
ou en pointant votre navigateur vers l'adresse http://127.0.0.1, ce qui vous retournera un certain nombre d'informations sur le serveur et la configuration php.

**Pour tester le service mysql :**


```Shell
bash-3.2$ mysql -h 127.0.0.1 -u sample -p sample  # ou l'@ip de la machine boot2docker
Enter password: 
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 1
...
```
## Configuration des services ##

### Configuration du service mydb ###

Le service mydb se configure dans le fichier Docker-compose.yml :

```
mydb:
  build: sql/
  ports:
    - 3306:3306
  environment:
    - DBNAME=sample
    - DBUSER=sample
    - DBPWD=sample
    - DBINIT=true
    - MYSQL_ROOT_PWD=root
```
ainsi que dans le répertoire sql : 

```Shell
bash-3.2$ ls sql
Dockerfile	database.sql	docker.run	entrypoint.sh
```

Il est possible de changer le port sur lequel le service est accessible au sein de la machine hôte. C'est notamment indispensable si plusieurs services mysql sont susceptibles de fonctionner sur cette machine (en particulier, si il y a déjà un serveur mysql installé et en focntionnement).

```
mydb:
  build: sql/
  ports:
    - 3307:3306
```

Les variables d'environnement permettent de définir une base de données à créer ainsi qu'un utilisateur, et d'éventuellement initialiser la base de données créée : 

- DBNAME=sample : si présent, création d'une base de données avec le nom fourni,
- DBUSER=sample / DBPWD=sample : si présent, création d'un utilisateur avec le nom/pwd fourni ; si une BD est créée, l'utilisateur créé obtient des droits complet sur cette base,
- DBINIT=true : exécute un script sql permettant d'initialiser la/les bases de données. Ce script est obligatoirement stocké dans le fichier sql/database.sql
- MYSQL\_ROOT_PWD=root : permet de définir le mot de passe de l'utilisateur root pour le serveur mysql

A noter que le service peut être utilisé seul : 

```Shell
bash-3.2$ cd sql
bash-3.2$ docker build -d mysqldb .
...
bash-3.2$ docker run -d -p 3306:3306 mysqldb
```

### Configuration du service http ###

le service http se configure dans le fichier docker-compose.yml :

```Shell
http:
  build: server/
  ports:
    - "80:80"
  volumes_from:
    - data
  volumes:
    - ./app:/var/app
  links:
    - mydb
  environment:
    - RUN_COMPOSER="yes"
```

Le serveur http est apache2 sur le port 80 avec : 
- le module url-rewrite activé
- AllowOverride All sur tous les répertoires pour permettre les directives .htaccess
- le document root est fixé à /var/www/html qui est un lien vers /var/app

PHP est installé avec les extensions : php5-mysql, php5-pgsql php5-cli php5-gd php5-curl php-pear php-apc. Git et composer sont installés.

Le service http est connecté au port 80 de la machine hôte. Il est recommandé de changer ce port si pluseurs services http sont susceptibles de fonctionner en même temps : 

```Shell
http:
  build: server/
  ports:
    - "8888:80"
```

__Configuration PHP :__ 

La configuration php est celle installée par le paquet apache-php5. Cette configuration est adaptée à un serveur en mode production. Pour adapter cette configuration, le contenu du fichier server/local.php.ini est ajouté à la fin du fichier /etc/php5/apache2/php.ini au sein du conteneur.

Le fichier server/local.php.ini fourni permet d'obtenir une configuration plus adaptée au développement : 

```Shell
error_reporting = -1
display_errors = On
display_startup_errors = On
track_errors = On
...
allow_url_fopen = Off
allow_url_include = Off
```
 

__Installation de l'application :__

Le service http monte le répertoire local ./app sur le chemin /var/app au sein du conteneur, c'est à dire le Document Root du serveur. L'usage est donc de déployer dans le répertoire local ./app l'ensemble de l'application php. Dans le répertoire fourni, un fichier .htacess redirige les requêtes vers le fichier sample/index.php.
```Shell
http:
  build: server/
  volumes:
    - ./app:/var/app
```

__Accès à la base de données :__ 

un lien est créé vers le service **mydb** :
```Shell
http:
  build: server/
  links:
    - mydb
```
Le **hostname** du serveur mysql à utiliser dans le code de l'application est donc **mydb**. 

__Accès à des données partagées :__ le service monte un volume géré par le service data. Le point de montage prévu est situé dans un répertoire en dessous du document root, ce qui rend les données montées accessibles par une url.
Le service data monte le répertoire local ./app-data sur le chemin /var/app/data dans le conteneur :


```Shell
data:
  image: ubuntu:14.04
  volumes:
    - ./app-data:/var/app/data

http:
  build: server/
  ports:
    - "80:80"
  volumes_from:
    - data
```

__Utilisation de composer__ Composer est installé dans le service http, ainsi que Git, ce qui permet de faire le déploiement, la résolution des dépendances et le contrôle des contraintes *à l'intérieur du conteneur*.

#####Pour utiliser Composer, 2 possibilités :######

**Démarrer un shell interactif** sur le conteneur en cours d'utilisation, et exécuter à la main composer :

```Shell
bash-3.2$ docker-compose ps
            Name                         Command              State            Ports          
---------------------------------------------------------------------------------------------
lampdevdockertemplate_data_1   /bin/bash                      Exit 0                          
lampdevdockertemplate_http_1   /run.sh                        Up       0.0.0.0:80->80/tcp     
lampdevdockertemplate_mydb_1   /usr/local/bin/entrypoint.sh   Up       0.0.0.0:3306->3306/tcp 
bash-3.2$ docker exec -t -i lampdevdockertemplate_http_1 /bin/bash
root@7665411c9ef0:/# cd /var/app
root@7665411c9ef0:/var/app# composer install
...
root@7665411c9ef0:exit
```

**Positionner la variable d'environnement** RUN\_COMPOSER dans le fichier docker-compose.yml. 

```Shell
http:
  build: server/
  ports:
    - "80:80"
  environment:
    - RUN_COMPOSER="yes"
```
Cette variable provoque l'exécution de la commande
```Shell
composer install
```
au moment de la création du conteneur du service http.

